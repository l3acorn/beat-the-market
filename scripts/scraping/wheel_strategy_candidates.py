import os
import glob
import datetime

import pandas as pd
import yfinance as yf


def get_latest_liquid_options(data_dir='data/options'):
    etf_option_files = glob.glob(os.path.join(data_dir, 'most_liquid_etfs_*.csv'))
    etf_option_files.sort()
    latest_file = etf_option_files[-1]
    return latest_file

etf_option_file = get_latest_liquid_options()
df_syms = pd.read_csv(etf_option_file)

df_opts = pd.DataFrame(columns=['ticker', 'price', 'date', 'type', 'strike',
                                'open_interest', 'last', 'ask', 'bid'])

# for symbol in df_syms.symbol:
# retrieve current option chain
symbol = df_syms.symbol.iloc[0]
# try to download symbol info if DNE

symbol_dir = os.path.join('data', 'tmp', symbol)
if os.path.exists(symbol_dir):
    pass
else:
    ticker = yf.Ticker(symbol)
    print('Downloading ticker info for {}'.format(symbol))
    ticker_info = ticker.info
    with open(os.path.join(symbol_dir, 'ticker_info.json'), 'w') as f:
        json.dump(f, ticker_info)
    '''
    option_dates = [datetime.datetime.fromisoformat(d).date() \
                    for d in ticker.options]

    print('Downloading option info for {}'.format(symbol))
    today_date = datetime.datetime.today().date()
    near_dates = [d for d in option_dates if (d - today_date).days >= 30 or \
                                             (d - today_date).days <= 45]
    '''
'''
ticker = yf.Ticker(symbol)
print('Downloading ticker info for {}'.format(symbol))
ticker_info = ticker.info
option_dates = [datetime.datetime.fromisoformat(d).date() \
                for d in ticker.options]

today_date = datetime.datetime.today().date()
near_dates = [d for d in option_dates if (d - today_date).days >= 30 or \
                                         (d - today_date).days <= 45]

# for date in near_dates:
near_date = near_dates[0]
opt_chain = ticker.option_chain(near_date.isoformat())
calls = opt_chain[0]
puts = opt_chain[1]

for idx,row in calls.iterrows():
    new_row = {'ticker': symbol, 
                'price': ticker_info['regularMarketPrice'],
                'date': near_date,
                'type': 'C',
                'strike': row.strike,
                'open_interest': row.openInterest,
                'last': row.lastPrice,
                'ask': row.ask,
                'bid': row.bid
                }
    df_opts = df_opts.append(new_row, ignore_index=True)
'''
