import os
from bs4 import BeautifulSoup
import pandas as pd

from selenium import webdriver
import datetime

options = webdriver.ChromeOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument('--incognito')
options.add_argument('--headless')
driver = webdriver.Chrome('/home/emily/_chromedriver_install/chromedriver', chrome_options=options)

#%%
url_dict = {
    'stocks' :  'https://www.barchart.com/options/most-active/stocks?page=all&viewName=main',
    'etfs' :    'https://www.barchart.com/options/most-active/etfs?page=all&viewName=main'
}

output_dir = os.path.join('data', 'options')
os.makedirs(output_dir, exist_ok=True)

for equity_type in url_dict.keys():
    url = url_dict[equity_type]

    driver.get(url)

    page_source = driver.page_source

    # parse with BeautifulSoup
    soup = BeautifulSoup(page_source, 'lxml')
    table_selector = soup.find_all('div', class_='bc-table-scrollable')
    rows = table_selector[0].find_all('a', attrs={'data-ng-class':'setTriggeredClass(row)'})

    symbols = [r.text for r in rows]

    df = pd.DataFrame(data={'symbol': symbols})

    # save to .csv with date
    today = datetime.datetime.today()
    today_str = '{0:4d}_{1:02d}_{2:02d}'.format(today.year, today.month, today.day)
    output_fn = os.path.join('data', 'options', 'most_liquid_{}_{}.csv'.format(equity_type, today_str))

    df.to_csv(output_fn)
