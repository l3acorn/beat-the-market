import pickle

import numpy as np
import pandas as pd
import yfinance as yf

from btm.portfolio import Portfolio, transfer_actions
from utils import get_latest_transaction_file, parse_option_symbol


with open('data/portfolio_obj.pkl', 'rb') as f:
    pf = pickle.load(f)


# TODO: determine commissions at different dates
# TODO: determine balance for selling shares
# TODO: factor in dividend payouts to cash balance
commission = 4.07 # average over portfolio lifetime


def buy_shares(ticker_data, ticker, date, cash, commission, df_buys, buy_count):
    cash, buy_dict = get_buy_amount(ticker_data, ticker, date, cash, commission)
    if buy_dict is not None:
        df_buys.loc[buy_count] = buy_dict
        buy_count += 1
    return cash, buy_count


def get_buy_amount(ticker_data, ticker, date, cash, commission):
    next_trade_date = ticker_data.index[ticker_data.index > np.datetime64(date)][0]
    price = ticker_data.loc[next_trade_date.date().isoformat()].Close
    n_shares = np.floor((cash-commission) / price)
    
    buy_dict = None
    if n_shares > 0:
        amount = (n_shares * price) + commission
        buy_dict = {'Date': date, 'Action': 'Buy', 'Symbol': ticker,
                'Quantity': int(n_shares), 'Price': price, 'Amount': -amount}
        cash -= amount

    return cash, buy_dict


def buy_fractional(ticker_data, ticker, date, cash, commission, df_buys, buy_count):
    buy_dict = get_fractional_amount(ticker_data, ticker, date, cash, 
            commission)
    df_buys.loc[buy_count] = buy_dict
    buy_count += 1
    return buy_count


def get_fractional_amount(ticker_data, ticker, date, cash, commission):
    next_trade_date = ticker_data.index[ticker_data.index > np.datetime64(date)][0]
    price = ticker_data.loc[next_trade_date.date().isoformat()].Close
    n_shares = (cash-commission) / price
    
    amount = (n_shares * price) + commission
    buy_dict = {'Date': date, 'Action': 'Buy', 'Symbol': ticker,
            'Quantity': int(n_shares), 'Price': price, 'Amount': -amount}

    return buy_dict


def build_portfolio(ticker, ticker_data, df_divs, orig_portfolio):
    
    df = orig_portfolio.transactions
    transfers = df[df.Action.isin(transfer_actions)]
    transfers.reset_index()
    
    # purchase at start date with initial balance
    df_buys = pd.DataFrame(columns=transfers.columns)

    cash = orig_portfolio.initial_balance
    date = orig_portfolio.start_date

    buy_count = 0
    cash, buy_count = buy_shares(ticker_data, ticker, date, cash, commission, 
            df_buys, buy_count)

    # buy shares at each transfer
    for idx in transfers.index:
        transfer_amount = transfers.loc[idx].Amount
        
        if not np.isnan(transfer_amount):
            cash += transfer_amount
            
            date = transfers.loc[idx].Date
            if cash > 0:
                cash, buy_count = buy_shares(ticker_data, ticker, date, cash, 
                        commission, df_buys, buy_count)

    # reinvest dividends
    start_date = orig_portfolio.start_date
    df_actions = df_divs[df_divs.index >= np.datetime64(start_date)]
    for idx in df_divs.index:
        dividend = df_divs.iloc[0].Dividends
        # TODO: combine dataframes for easier processing?

    transactions = transfers.append(df_buys)

    portfolio = Portfolio()
    portfolio.transactions = transactions
    portfolio.set_initial_balance(874.)
    portfolio.start_date = orig_portfolio.start_date
    portfolio.end_date = orig_portfolio.end_date

    return portfolio


pf_start = pf.start_date
pf_end = pf.end_date


tickers = ['VTI'] # ['VNQ', 'SPY', 'VTI', 'QQQ', 'TSLA']
portfolios = []

'''
for ticker in tickers:
    print('determining {} portfolio balance...'.format(ticker))
    ticker_data = yf.download(ticker, start=pf_start.isoformat(), 
                        end=pf_end.isoformat())
    ticker_actions = yf.Ticker(ticker).actions
    ticker_pf = build_portfolio(ticker, ticker_data, ticker_actions, pf)
    portfolios.append(ticker_pf)

for ticker, portfolio in zip(tickers, portfolios):
    balance = portfolio.calculate_balance()
    print('{} portfolio balance: \t{}'.format(ticker, balance))

print('determining personal portfolio balance...')
curr_balance = pf.calculate_balance()
print(curr_balance)
'''
df = pf.transactions
transfers = df[df.Action.isin(transfer_actions)]

vti = yf.Ticker('VTI')
# TODO: use history to get dividends as well as prices
actions = vti.actions
actions = actions.reset_index()
actions = actions[actions.Date >= np.datetime64(pf_start)]

df_total = transfers.append(actions)
df_total.sort_values('Date', inplace=True, ascending=True)


# ripped from build_portfolio
orig_portfolio = pf
ticker = 'VTI'
ticker_data = yf.download(ticker, start=pf_start.isoformat(), 
                    end=pf_end.isoformat())
# purchase at start date with initial balance
df_buys = pd.DataFrame(columns=transfers.columns)

cash = orig_portfolio.initial_balance
date = orig_portfolio.start_date

buy_count = 0
cash, buy_count = buy_shares(ticker_data, ticker, date, cash, commission, 
        df_buys, buy_count)


for idx in df_total.index:
    # determine if transfer or dividend
    transfer_amount = df_total.loc[idx].Amount
    dividend_per_share = df_total.loc[idx].Dividends

    if np.isnan(dividend_per_share) and not np.isnan(transfer_amount):
    elif np.isnan(dividend_per_share) and not np.isnan(transfer_amount):


'''
# buy shares at each transfer
for idx in transfers.index:
    transfer_amount = transfers.loc[idx].Amount
    
    if not np.isnan(transfer_amount):
        cash += transfer_amount
        
        date = transfers.loc[idx].Date
        if cash > 0:
            cash, buy_count = buy_shares(ticker_data, ticker, date, cash, 
                    commission, df_buys, buy_count)

# reinvest dividends
start_date = orig_portfolio.start_date
df_actions = df_divs[df_divs.index >= np.datetime64(start_date)]
for idx in df_divs.index:
    dividend = df_divs.iloc[0].Dividends
    # TODO: combine dataframes for easier processing?

transactions = transfers.append(df_buys)

portfolio = Portfolio()
portfolio.transactions = transactions
portfolio.set_initial_balance(874.)
portfolio.start_date = orig_portfolio.start_date
portfolio.end_date = orig_portfolio.end_date
'''
