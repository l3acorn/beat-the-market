import warnings
import yfinance as yf

import pandas as pd
from utils import preprocess_dates, preprocess_dollar_values, \
        negate_sell_quantities, fix_symbol_changes, \
        parse_option_symbol, is_option_symbol


buy_sell_actions = ['Buy to Close', 'Sell to Open', 'Buy', 'Sell', 
        'Reinvest Shares', 'Buy to Open', 'Exchange or Exercise', 'Assigned', 
        'Sell to Close'
]
# n.b. some journal actions are not transfers, check 'amount' field
transfer_actions = ['MoneyLink Deposit','Journal','Wire Funds Received',
        'MoneyLink Transfer', 'Security Transfer','Funds Received'
]

div_int_actions = ['Bank Interest','Cash Dividend','Qualified Dividend', 
    'Pr Yr Cash Div','Margin Interest', 'Qual Div Reinvest',
    'Long Term Cap Gain Reinvest', 'Service Fee','ADR Mgmt Fee', 
    'Foreign Tax Paid','Pr Yr Div Reinvest'
]
# other possible actions:	
# ['Reinvest Dividend', 'Name Change', 'Stock Merger']


class Portfolio:

    def __init__(self, initial_balance=0.):

        self.initial_balance = initial_balance
        
        self.transactions = None # pandas DataFrame storing transaction info
        self.start_date = None
        self.end_date = None
        
        self.current_holdings = {}
        # TODO: refresh current prices if out of date
        self.__curent_prices = None
        self.balance = 0.
        self.margin = 0.
        self.roi = 0.


    def from_csv(csv_file):
        ''' Static method. Build a list of transactions from a .csv file
        '''
        df = pd.read_csv(csv_file, header=1)
        
        # filter unneeded columns
        for colname in df.columns:
            if 'Unnamed' in colname:
                df.drop(columns=[colname], inplace=True)

        # fees and commisions are already calculated into amount, description
        # doesn't give additional info
        df.drop(columns=['Fees & Comm', 'Description'], inplace=True)
        
        preprocess_dates(df)
        preprocess_dollar_values(df)
        negate_sell_quantities(df)
        fix_symbol_changes(df)

        portfolio = Portfolio()
        portfolio.transactions = df
        portfolio.start_date = portfolio.transactions.Date.min()
        portfolio.end_date = portfolio.transactions.Date.max()

        portfolio.__determine_holdings()
        
        return portfolio


    def set_initial_balance(self, initial_balance):
        self.initial_balance = initial_balance


    def get_holdings(self, to_date=None):
        ''' Based on this portfolio's transaction list, determine
        which securities have been bought and not yet sold.
        '''
        df = self.transactions
        if to_date is None:
            to_date = self.end_date
        df_indates = df[df['Date'] <= to_date]
        
        holdings = {}
        symbols = df_indates.Symbol.unique()

        for symbol in symbols:
            buy_sells = df_indates[df_indates.Action.isin(buy_sell_actions)]
            quantity = buy_sells[buy_sells.Symbol == symbol].Quantity.sum()

            if abs(quantity) > 1e-10:
                holdings[symbol] = quantity

        return holdings 


    def __determine_holdings(self):
        ''' Determine current holdings in this portfolio and
        store in this Portfolio's holdings dictionary.
        '''
        holdings = self.get_holdings()
        self.current_holdings = holdings


    def calculate_cash(self, to_date=None):
        df = self.transactions
        if to_date is None:
            to_date = self.end_date
        df_indates = df[df['Date'] <= to_date]
        return df_indates.Amount.sum() + self.initial_balance
    

    def calculate_balance(self):# TODO: date=None):
        ''' Calculate the balance of this portfolio on a given date
        (or the end date if not provided), based on current holdings'
        values and cash balance.
        '''
        holdings = self.get_holdings()

        if self.__curent_prices is None:
            symbol_lst = list(holdings.keys())
            underlying_symbols = list(set([s.split(' ')[0] for s in symbol_lst]))

            value_dict = {}

            for symbol in underlying_symbols:
                positions = [s for s in symbol_lst if s.startswith(symbol)]
                ticker = yf.Ticker(symbol)

                for pos in positions:
                    if is_option_symbol(pos):
                        _, date, strike, callput = parse_option_symbol(pos)
                        date_str = date.isoformat().split('T')[0]   
                        option_chain = ticker.option_chain(date_str)
                        if callput == 'C':
                            df_opt = option_chain.calls
                        else:
                            df_opt = option_chain.puts

                        row_idx = df_opt.index[df_opt['strike'] == strike][0]
                        price = 100 * df_opt.iloc[row_idx].lastPrice

                    else:
                        # assume not an option, get the current market value
                        price = ticker.info['regularMarketPrice']
                    
                    value_dict[pos] = price
            
            self.__curent_prices = value_dict

        else:
            value_dict = self.__curent_prices

        assert set(holdings.keys()) == set(value_dict.keys())  

        total_value = 0
        for k in holdings.keys():
            quantity = holdings[k]
            value = value_dict[k]
            total_value += (quantity * value)

        cash = self.calculate_cash()
        return cash + total_value
        

    def calculate_roi(self, from_date=None, to_date=None):
        # TODO: use formula from https://www.investopedia.com/terms/t/time-weightedror.asp
        warnings.warn('Portfolio.calculate_roi not implemented yet.')
        
        df = self.transactions
        if from_date is None:
            from_date = self.start_date

        if to_date is None:
            to_date = self.end_date
        
        # mult = 1.
        # for each month from from_date to to_date:
            # calculate initial_value, end_value, cash_flow 
            # initial_value = self.calculate_balance(from_date)     
            # end_value = self.calculate_balance(to_date)     
            # cash_flow = self.net_investment(from_date=from_date, to_date=to_date)
            # P_i = (end_value - initial_value + cash_flow) / (initial_value + cash_flow)
            # mult = mult * (1 + P_i)
        # return mult - 1


    def net_investment(self, from_date=None, to_date=None):
        ''' Calculate the net investment into this portfolio between
        the two given dates. Use self.start_date and self.end_date if not 
        provided.
        '''
        df = self.transactions
        if from_date is None:
            from_date = self.start_date

        if to_date is None:
            to_date = self.end_date

        df_indates = df[(df['Date'] >= from_date) & (df['Date'] <= to_date)]
        net = df_indates[df_indates.Action.isin(transfer_actions)].Amount.sum()
        return net
